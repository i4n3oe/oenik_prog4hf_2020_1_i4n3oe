﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Sexshop.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:58142/api/ToysApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "ToyResult");
        }

        public List<ToyVM> ApiGetToys()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<ToyVM>>(json);
            // SendMessage(true);
            return list;
        }

        public void ApiDelToy(ToyVM toy)
        {
            bool success = false;
            if (toy != null)
            {
                string json = client.GetStringAsync(url + "del/" + toy.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        private bool ApiEditToy(ToyVM toy, bool isEditing)
        {
            if (toy == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(ToyVM.Id), toy.Id.ToString());
            postData.Add(nameof(ToyVM.Brand), toy.Brand);
            postData.Add(nameof(ToyVM.Name), toy.Name);
            postData.Add(nameof(ToyVM.Price), toy.Price.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditToy(ToyVM toy, Func<ToyVM, bool> editor)
        {
            ToyVM clone = new ToyVM();
            if (toy != null) clone.CopyFrom(toy);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (toy != null) success = ApiEditToy(clone, true);
                else success = ApiEditToy(clone, false);
            }
            SendMessage(success == true);
        }
    }
}
