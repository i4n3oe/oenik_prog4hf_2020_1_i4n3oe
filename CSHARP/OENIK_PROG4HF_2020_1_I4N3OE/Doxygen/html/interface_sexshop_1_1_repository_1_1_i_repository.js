var interface_sexshop_1_1_repository_1_1_i_repository =
[
    [ "Create", "interface_sexshop_1_1_repository_1_1_i_repository.html#a089fa78e5312a11b9575a678421dbf42", null ],
    [ "CustomerData", "interface_sexshop_1_1_repository_1_1_i_repository.html#a5f31d9a7d970be26fe6686f524bed40a", null ],
    [ "Delete", "interface_sexshop_1_1_repository_1_1_i_repository.html#a4ee3ca4b6de690c4718fcf90c5b27830", null ],
    [ "ProductData", "interface_sexshop_1_1_repository_1_1_i_repository.html#a134ed037fd3d7d777121d35129a1a119", null ],
    [ "PurchaseData", "interface_sexshop_1_1_repository_1_1_i_repository.html#a574b6e55e72d2500455d0d37e0ca7b00", null ],
    [ "Read", "interface_sexshop_1_1_repository_1_1_i_repository.html#a1072cab6c8a32f1be582d8d1e086d00a", null ],
    [ "Read", "interface_sexshop_1_1_repository_1_1_i_repository.html#ad315484d894d7e22cd7be506511faa1e", null ],
    [ "ReviewData", "interface_sexshop_1_1_repository_1_1_i_repository.html#a97ad926a7a4635b62d161f54fce09489", null ],
    [ "Update", "interface_sexshop_1_1_repository_1_1_i_repository.html#a388e5f415d3085e65c8832ee47071c32", null ]
];