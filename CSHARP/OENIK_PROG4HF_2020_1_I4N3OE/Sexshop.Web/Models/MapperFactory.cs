﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sexshop.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Sexshop.Data.PRODUCT, Sexshop.Web.Models.Toy>().
                ForMember(dest => dest.ID, map => map.MapFrom(src => src.BARCODE)).
                ForMember(dest => dest.Name, map => map.MapFrom(src => src.NAME)).
                ForMember(dest => dest.Brand, map => map.MapFrom(src => src.BRAND)).
                ForMember(dest => dest.Price, map => map.MapFrom(src => src.PRICE));
            });
            return config.CreateMapper();
        }
    }
}