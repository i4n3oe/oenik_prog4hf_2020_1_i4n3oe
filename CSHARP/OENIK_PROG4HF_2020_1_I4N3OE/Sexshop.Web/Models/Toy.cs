﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sexshop.Web.Models
{
    public class Toy
    {
        [Display(Name = "Toy ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Toy Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Toy Brand")]
        [Required]
        public string Brand { get; set; }

        [Display(Name = "Toy Price")]
        [Required]
        public int Price { get; set; }
    }
}