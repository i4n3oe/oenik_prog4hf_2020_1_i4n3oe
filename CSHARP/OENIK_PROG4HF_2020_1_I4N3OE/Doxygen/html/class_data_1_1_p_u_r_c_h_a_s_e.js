var class_data_1_1_p_u_r_c_h_a_s_e =
[
    [ "BARCODE", "class_data_1_1_p_u_r_c_h_a_s_e.html#a6a7858ce51d5def113da4bdef7bfcade", null ],
    [ "CUSTOMER", "class_data_1_1_p_u_r_c_h_a_s_e.html#a717ef07a7f826bcb81e425334adff53e", null ],
    [ "CUSTOMERID", "class_data_1_1_p_u_r_c_h_a_s_e.html#a964233631b1d845843ce67360317a926", null ],
    [ "MESSAGE", "class_data_1_1_p_u_r_c_h_a_s_e.html#a3293f4f337c4c653a4ef70fb3669484e", null ],
    [ "ORDERDATE", "class_data_1_1_p_u_r_c_h_a_s_e.html#ac77cac8b3cd4b4c3db602679456c486b", null ],
    [ "PRIVACY", "class_data_1_1_p_u_r_c_h_a_s_e.html#a4d6929a72facdde4bcbc3af35516c268", null ],
    [ "PRODUCT", "class_data_1_1_p_u_r_c_h_a_s_e.html#a065f836f08c392ec9704d7dc6c1f9b6a", null ],
    [ "PURCHASEID", "class_data_1_1_p_u_r_c_h_a_s_e.html#a0bbb75e17e3ab5f83f801a37af111e28", null ]
];