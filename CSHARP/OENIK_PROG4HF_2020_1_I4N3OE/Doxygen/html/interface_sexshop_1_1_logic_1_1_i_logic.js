var interface_sexshop_1_1_logic_1_1_i_logic =
[
    [ "Create", "interface_sexshop_1_1_logic_1_1_i_logic.html#aa6bc6639c18c2d2c9c87816829c8bd7c", null ],
    [ "Delete", "interface_sexshop_1_1_logic_1_1_i_logic.html#aef2aede8b66fac8d958c09bbbc8e0f26", null ],
    [ "GetToyColorFromJavaEndPoint", "interface_sexshop_1_1_logic_1_1_i_logic.html#a687c97d7618c0e95eed0f9ecd28ad208", null ],
    [ "MostActiveReviewer", "interface_sexshop_1_1_logic_1_1_i_logic.html#aa372d6875a1a5b874bef783d52d508e5", null ],
    [ "ReadAllTable", "interface_sexshop_1_1_logic_1_1_i_logic.html#a098078bbf1d79e0ca8bbfe8ec7de4a6b", null ],
    [ "ReadItem", "interface_sexshop_1_1_logic_1_1_i_logic.html#a48945ca3a2943400b293de6dc48e5de1", null ],
    [ "ReadTable", "interface_sexshop_1_1_logic_1_1_i_logic.html#a39546b4e15ce00c5f234369ea98d361d", null ],
    [ "SexToysSoldByYear", "interface_sexshop_1_1_logic_1_1_i_logic.html#a9bf7f347ed5b19fff43db6fbc347e820", null ],
    [ "TopSellerSexToy", "interface_sexshop_1_1_logic_1_1_i_logic.html#a1ef355e7f7a2189f9c732024e68b229b", null ],
    [ "Update", "interface_sexshop_1_1_logic_1_1_i_logic.html#a72d30286be0fd059f59096946ef9f6f1", null ]
];