var hierarchy =
[
    [ "Data.CUSTOMER", "class_data_1_1_c_u_s_t_o_m_e_r.html", null ],
    [ "DbContext", null, [
      [ "Data::SexshopDbEntities", "class_data_1_1_sexshop_db_entities.html", null ]
    ] ],
    [ "Sexshop.Logic.ILogic", "interface_sexshop_1_1_logic_1_1_i_logic.html", [
      [ "Sexshop.Logic.Logic", "class_sexshop_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "Sexshop.Repository.IRepository", "interface_sexshop_1_1_repository_1_1_i_repository.html", [
      [ "Sexshop.Repository.Repo", "class_sexshop_1_1_repository_1_1_repo.html", null ]
    ] ],
    [ "Sexshop.Logic.Tests.LogicTest", "class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "Data.PRODUCT", "class_data_1_1_p_r_o_d_u_c_t.html", null ],
    [ "Data.PURCHASE", "class_data_1_1_p_u_r_c_h_a_s_e.html", null ],
    [ "Data.REVIEW", "class_data_1_1_r_e_v_i_e_w.html", null ]
];