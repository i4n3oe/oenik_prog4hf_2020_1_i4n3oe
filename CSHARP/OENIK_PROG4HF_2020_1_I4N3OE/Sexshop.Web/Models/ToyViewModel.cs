﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sexshop.Web.Models
{
    public class ToyViewModel
    {
        public Toy EditedToy { get; set; }
        public List<Toy> ListOfToys { get; set; }
    }
}