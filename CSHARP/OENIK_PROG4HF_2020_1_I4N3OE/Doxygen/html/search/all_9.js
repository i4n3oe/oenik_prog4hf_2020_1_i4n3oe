var searchData=
[
  ['read_20',['Read',['../interface_sexshop_1_1_repository_1_1_i_repository.html#ad315484d894d7e22cd7be506511faa1e',1,'Sexshop.Repository.IRepository.Read(string table, int id)'],['../interface_sexshop_1_1_repository_1_1_i_repository.html#a1072cab6c8a32f1be582d8d1e086d00a',1,'Sexshop.Repository.IRepository.Read(string table)'],['../class_sexshop_1_1_repository_1_1_repo.html#accc9cda315a4152b6019c2001f318879',1,'Sexshop.Repository.Repo.Read(string table, int id)'],['../class_sexshop_1_1_repository_1_1_repo.html#ad77b7831c8e1186d04207b02e1844173',1,'Sexshop.Repository.Repo.Read(string table)']]],
  ['readalltable_21',['ReadAllTable',['../interface_sexshop_1_1_logic_1_1_i_logic.html#a098078bbf1d79e0ca8bbfe8ec7de4a6b',1,'Sexshop.Logic.ILogic.ReadAllTable()'],['../class_sexshop_1_1_logic_1_1_logic.html#a1fd58e6a7f07bd76353290e1b0349038',1,'Sexshop.Logic.Logic.ReadAllTable()']]],
  ['readitem_22',['ReadItem',['../interface_sexshop_1_1_logic_1_1_i_logic.html#a48945ca3a2943400b293de6dc48e5de1',1,'Sexshop.Logic.ILogic.ReadItem()'],['../class_sexshop_1_1_logic_1_1_logic.html#a67c0fd78fa68091e43163523e3bd634a',1,'Sexshop.Logic.Logic.ReadItem()']]],
  ['readtable_23',['ReadTable',['../interface_sexshop_1_1_logic_1_1_i_logic.html#a39546b4e15ce00c5f234369ea98d361d',1,'Sexshop.Logic.ILogic.ReadTable()'],['../class_sexshop_1_1_logic_1_1_logic.html#aa176c93b5da5e27f334098105a7dd7ea',1,'Sexshop.Logic.Logic.ReadTable()']]],
  ['repo_24',['Repo',['../class_sexshop_1_1_repository_1_1_repo.html',1,'Sexshop::Repository']]],
  ['review_25',['REVIEW',['../class_data_1_1_r_e_v_i_e_w.html',1,'Data']]],
  ['reviewdata_26',['ReviewData',['../interface_sexshop_1_1_repository_1_1_i_repository.html#a97ad926a7a4635b62d161f54fce09489',1,'Sexshop.Repository.IRepository.ReviewData()'],['../class_sexshop_1_1_repository_1_1_repo.html#a5a8a66b18e51a0d462785d72552098bc',1,'Sexshop.Repository.Repo.ReviewData()']]]
];
