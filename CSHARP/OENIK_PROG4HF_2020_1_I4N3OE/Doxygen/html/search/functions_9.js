var searchData=
[
  ['test_5fcreate_74',['Test_Create',['../class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a4a166b60c4ec76d840c97c956e3fe9ff',1,'Sexshop::Logic::Tests::LogicTest']]],
  ['test_5fdelete_75',['Test_Delete',['../class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a7015518e79bfb99ff2c6d98ba1ccb59e',1,'Sexshop::Logic::Tests::LogicTest']]],
  ['test_5fmostactivereviewer_76',['Test_MostActiveReviewer',['../class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#aa212038fa2e4b0e143bb736ee90eac81',1,'Sexshop::Logic::Tests::LogicTest']]],
  ['test_5fread_77',['Test_Read',['../class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a3b4e926756d45535735a326ce2c9d98b',1,'Sexshop::Logic::Tests::LogicTest']]],
  ['test_5freadalltable_78',['Test_ReadAllTable',['../class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a78de3e7fa4ff9566d97b28d4b0de77b7',1,'Sexshop::Logic::Tests::LogicTest']]],
  ['test_5freadtable_79',['Test_ReadTable',['../class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#adb61928032be802722b3ed13562ca97c',1,'Sexshop::Logic::Tests::LogicTest']]],
  ['test_5ftopsellertoy_80',['Test_TopSellerToy',['../class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a24b427b58fb3adf03b8f06992b33b647',1,'Sexshop::Logic::Tests::LogicTest']]],
  ['test_5fupdate_81',['Test_Update',['../class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a42d409a4055743d117c263389de6673c',1,'Sexshop::Logic::Tests::LogicTest']]],
  ['topsellersextoy_82',['TopSellerSexToy',['../interface_sexshop_1_1_logic_1_1_i_logic.html#a1ef355e7f7a2189f9c732024e68b229b',1,'Sexshop.Logic.ILogic.TopSellerSexToy()'],['../class_sexshop_1_1_logic_1_1_logic.html#a45253557b82f7c6222ae3294c97b11df',1,'Sexshop.Logic.Logic.TopSellerSexToy()']]]
];
