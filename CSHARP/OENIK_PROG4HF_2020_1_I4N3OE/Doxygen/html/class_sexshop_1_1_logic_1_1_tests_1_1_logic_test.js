var class_sexshop_1_1_logic_1_1_tests_1_1_logic_test =
[
    [ "Init", "class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a9888efe5789a18866d9d454feb18c907", null ],
    [ "Test_Create", "class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a4a166b60c4ec76d840c97c956e3fe9ff", null ],
    [ "Test_Delete", "class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a7015518e79bfb99ff2c6d98ba1ccb59e", null ],
    [ "Test_MostActiveReviewer", "class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#aa212038fa2e4b0e143bb736ee90eac81", null ],
    [ "Test_Read", "class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a3b4e926756d45535735a326ce2c9d98b", null ],
    [ "Test_ReadAllTable", "class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a78de3e7fa4ff9566d97b28d4b0de77b7", null ],
    [ "Test_ReadTable", "class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#adb61928032be802722b3ed13562ca97c", null ],
    [ "Test_TopSellerToy", "class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a24b427b58fb3adf03b8f06992b33b647", null ],
    [ "Test_Update", "class_sexshop_1_1_logic_1_1_tests_1_1_logic_test.html#a42d409a4055743d117c263389de6673c", null ]
];