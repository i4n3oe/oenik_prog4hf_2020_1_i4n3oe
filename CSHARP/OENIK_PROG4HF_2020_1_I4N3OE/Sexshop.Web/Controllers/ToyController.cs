﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sexshop.Logic;
using AutoMapper;
using Sexshop.Web.Models;
using Sexshop.Data;
using Sexshop.Repository;

namespace Sexshop.Web.Controllers
{
    enum Tables { CUSTOMER, PRODOUCT, REVIEW, PURCHASE };

    public class ToyController : Controller
    {
        ILogic logic;
        IMapper mapper;
        ToyViewModel vm;

        public ToyController()
        {
            logic = new ToyLogic();
            mapper = MapperFactory.CreateMapper();

            vm = new ToyViewModel();
            vm.EditedToy = new Toy();
            var toys = logic.ReadProducts();
            vm.ListOfToys = mapper.Map<IList<Data.PRODUCT>, List<Models.Toy>>(toys);
        }

        private Toy GetToyModel(int id)
        {
            PRODUCT toy = logic.ReadProduct(id);
            return mapper.Map<Data.PRODUCT,Models.Toy>(toy);
        }

        // GET: Toy
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("ToyIndex", vm);
        }

        // GET: Toy/Details/5
        public ActionResult Details(int id)
        {
            return View("ToyDetails", GetToyModel(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (logic.DeleteProduct(id)) TempData["editResult"] = "Delete OK";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedToy = GetToyModel(id);
            return View("ToyIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Toy toy, string editAction)
        {
            if(ModelState.IsValid && toy != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    logic.CreateProduct(toy.ID, toy.Name, toy.Brand, toy.Price);
                }
                else
                {
                    bool success = logic.UpdateProduct(toy.ID, toy.Name, toy.Brand, toy.Price);
                    if(!success) TempData["editResult"] = "Edit FAIL";
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedToy = toy;
                return View("ToyIndex", vm);
            }
        }
    }
}
