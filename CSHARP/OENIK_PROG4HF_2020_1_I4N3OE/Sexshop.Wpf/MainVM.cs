﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Sexshop.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
		private ToyVM selectedToy;
		private ObservableCollection<ToyVM> allToys;

		public ObservableCollection<ToyVM> AllToys
		{
			get { return allToys; }
			set { Set(ref allToys, value); }
		}


		public ToyVM SelectedToy
		{
			get { return selectedToy; }
			set { Set(ref selectedToy, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<ToyVM, bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();

			DelCmd = new RelayCommand(() => logic.ApiDelToy(selectedToy));
			AddCmd = new RelayCommand(() => logic.EditToy(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditToy(selectedToy, EditorFunc));
			LoadCmd = new RelayCommand(() => AllToys = new ObservableCollection<ToyVM>(logic.ApiGetToys()));
		}
	}
}
