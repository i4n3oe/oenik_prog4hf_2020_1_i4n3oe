var class_sexshop_1_1_logic_1_1_logic =
[
    [ "Logic", "class_sexshop_1_1_logic_1_1_logic.html#ac618a81d8ed9ea080d751dcffd5269b1", null ],
    [ "Logic", "class_sexshop_1_1_logic_1_1_logic.html#aa689bd9e5b79e04c0dbfdef93fbbe799", null ],
    [ "Create", "class_sexshop_1_1_logic_1_1_logic.html#ac4955e498143ea351bae441cb779ba6f", null ],
    [ "Delete", "class_sexshop_1_1_logic_1_1_logic.html#a5a5b82271498225d1091c5e90114abf2", null ],
    [ "GetToyColorFromJavaEndPoint", "class_sexshop_1_1_logic_1_1_logic.html#af8ca0b6fceb05f0614c3d04524ff159d", null ],
    [ "MostActiveReviewer", "class_sexshop_1_1_logic_1_1_logic.html#a5d9eda79f8d1c37f09f38a1bbac539ee", null ],
    [ "ReadAllTable", "class_sexshop_1_1_logic_1_1_logic.html#a1fd58e6a7f07bd76353290e1b0349038", null ],
    [ "ReadItem", "class_sexshop_1_1_logic_1_1_logic.html#a67c0fd78fa68091e43163523e3bd634a", null ],
    [ "ReadTable", "class_sexshop_1_1_logic_1_1_logic.html#aa176c93b5da5e27f334098105a7dd7ea", null ],
    [ "SexToysSoldByYear", "class_sexshop_1_1_logic_1_1_logic.html#a8a0e6fc64e2da12b0372883dde012c99", null ],
    [ "TopSellerSexToy", "class_sexshop_1_1_logic_1_1_logic.html#a45253557b82f7c6222ae3294c97b11df", null ],
    [ "Update", "class_sexshop_1_1_logic_1_1_logic.html#a084b8a132e69d127f377a49ddb265531", null ]
];