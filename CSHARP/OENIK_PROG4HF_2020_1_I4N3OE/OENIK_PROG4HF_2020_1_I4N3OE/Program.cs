﻿// <copyright file="Program.cs" company="I4N3OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_2019_2_I4N3OE
{
    using System;
    using Sexshop.Logic;

    /// <summary>
    /// This is the main class.
    /// </summary>
    internal class Program
    {
        private static readonly string[] MainMenuOptions =
        {
                "Create", "Read", "Update", "Delete", "Spec", "Escape",
        };

        private static readonly string[] SubMenuOptions =
        {
                "Customer", "Product", "Review", "Purchase", "Escape",
        };

        private static readonly string[] SpecialMenuOptions =
       {
                "Top selled toy", "Most active reviewer", "Sex toys sold by year", "Toy color from java", "Escape",
       };

        private static readonly string[] ReadMenuOptions =
       {
                "Read an Entity", "Read a Table", "Read Database", "Escape",
       };

        private static readonly string[] ToyMenuOptions =
       {
                "HOLE DIGGER 2000", "STATIC ROD 1", "STICK OF JUSTICE", "FUCKMASTER PRO 5000", "HELPING HAND", "CRÉME DE LA MEOW MEOW", "TINY TIM", "PUSSY WITH MOANER", "Escape",
       };

        /// <summary>
        /// This is the entry point.
        /// </summary>
        /// <param name="args">Default value.</param>
        private static void Main(string[] args)
        {
            ILogic logic = new ToyLogic();
            int exit = 0;
            while (exit != -1)
            {
                exit = Menu(logic);
            }
        }

        private static int Menu(ILogic logic)
        {
            int mainMenuInput = 0;
            MenuGenerator("Mainmenu", MainMenuOptions);
            mainMenuInput = int.Parse(Console.ReadLine());
            Console.Clear();

            switch (mainMenuInput)
            {
                case 1: MenuGenerator("Create Menu", SubMenuOptions); break;
                case 2: MenuGenerator("Read Menu", SubMenuOptions); break;
                case 3: MenuGenerator("Update Menu", SubMenuOptions); break;
                case 4: MenuGenerator("Delete Menu", SubMenuOptions); break;
                case 5: MenuGenerator("Special Menu", SpecialMenuOptions); break;
                case 6: return -1;
                default: throw new Exception();
            }

            int subMenuInput = 0;
            subMenuInput = int.Parse(Console.ReadLine());

            switch (mainMenuInput)
            {
                case 1:
                    logic.Create(SubMenuOptions[subMenuInput - 1].ToUpper(), GetArgs(subMenuInput)); break;
                case 2:
                    MenuGenerator("Read Type", ReadMenuOptions);
                    switch (int.Parse(Console.ReadLine()))
                    {
                        //case 1: Console.WriteLine(logic.ReadItem(SubMenuOptions[subMenuInput - 1].ToUpper(), GetID())); break;
                        //case 2: Console.WriteLine(logic.ReadTable(SubMenuOptions[subMenuInput - 1].ToUpper())); break;
                        case 3: Console.WriteLine(logic.ReadAllTable()); break;
                        case 4: return -1;
                        default: throw new Exception();
                    }

                    break;
                case 3:
                    logic.Update(SubMenuOptions[subMenuInput - 1].ToUpper(), GetArgs(subMenuInput)); break;
                case 4:
                    logic.Delete(SubMenuOptions[subMenuInput - 1].ToUpper(), GetID()); break;
                case 5:
                    switch (subMenuInput)
                    {
                        case 1: Console.WriteLine(logic.TopSellerSexToy()); break;
                        case 2: Console.WriteLine(logic.MostActiveReviewer()); break;
                        case 3: Console.WriteLine(logic.SexToysSoldByYear()); break;
                        case 4:
                            MenuGenerator("Toy Menu", ToyMenuOptions);
                            Console.WriteLine(logic.GetToyColorFromJavaEndPoint(ToyMenuOptions[int.Parse(Console.ReadLine()) - 1])); break;
                        case 5: return -1;
                        default: throw new Exception();
                    }

                    break;
                case 6: return -1;
                default: throw new Exception();
            }

            return 0;
        }

        private static void MenuGenerator(string title, string[] options)
        {
            Console.WriteLine("---------------------------------------------");
            Console.WriteLine(title);
            for (int i = 0; i < options.Length; i++)
            {
                Console.WriteLine((i + 1) + " - " + options[i]);
            }

            Console.WriteLine("---------------------------------------------");
        }

        private static int GetID()
        {
            Console.Write("Give the ID: ");
            return int.Parse(Console.ReadLine());
        }

        private static string[] GetArgs(int option)
        {
            string[] args;
            switch (option)
            {
                case 1:
                    args = new string[6];
                    Console.Write("CUSTOMERID: ");
                    args[0] = Console.ReadLine();
                    Console.Write("NAME: ");
                    args[1] = Console.ReadLine();
                    Console.Write("MAIL: ");
                    args[2] = Console.ReadLine();
                    Console.Write("ADDRESS: ");
                    args[3] = Console.ReadLine();
                    Console.Write("PHONE: ");
                    args[4] = Console.ReadLine();
                    Console.Write("BIRTHDATE: ");
                    args[5] = Console.ReadLine();
                    return args;
                case 2:
                    args = new string[6];
                    Console.Write("BARCODE: ");
                    args[0] = Console.ReadLine();
                    Console.Write("NAME: ");
                    args[1] = Console.ReadLine();
                    Console.Write("BRAND: ");
                    args[2] = Console.ReadLine();
                    Console.Write("PRICE: ");
                    args[3] = Console.ReadLine();
                    Console.Write("STOCK: ");
                    args[4] = Console.ReadLine();
                    Console.Write("STATUS: ");
                    args[5] = Console.ReadLine();
                    return args;
                case 3:
                    args = new string[7];
                    Console.Write("REVIEWID: ");
                    args[0] = Console.ReadLine();
                    Console.Write("CUSTOMERID: ");
                    args[1] = Console.ReadLine();
                    Console.Write("BARCODE: ");
                    args[2] = Console.ReadLine();
                    Console.Write("TITLE: ");
                    args[3] = Console.ReadLine();
                    Console.Write("MESSAGE: ");
                    args[4] = Console.ReadLine();
                    Console.Write("CREATED: ");
                    args[5] = Console.ReadLine();
                    Console.Write("RATING: ");
                    args[6] = Console.ReadLine();
                    return args;
                case 4:
                    args = new string[6];
                    Console.Write("PURCHASEID: ");
                    args[0] = Console.ReadLine();
                    Console.Write("CUSTOMERID: ");
                    args[1] = Console.ReadLine();
                    Console.Write("BARCODE: ");
                    args[2] = Console.ReadLine();
                    Console.Write("ORDERDATE: ");
                    args[3] = Console.ReadLine();
                    Console.Write("MESSAGE: ");
                    args[4] = Console.ReadLine();
                    Console.Write("PRIVACY: ");
                    args[5] = Console.ReadLine();
                    return args;
                default: throw new Exception();
            }
        }

        private void ItemToString(string[] item)
        {
        }
    }
}