var class_data_1_1_p_r_o_d_u_c_t =
[
    [ "PRODUCT", "class_data_1_1_p_r_o_d_u_c_t.html#a0ea20cf26bb7e8375195891768dd7326", null ],
    [ "BARCODE", "class_data_1_1_p_r_o_d_u_c_t.html#a745329a44a05d055e4eb6504182e695d", null ],
    [ "BRAND", "class_data_1_1_p_r_o_d_u_c_t.html#ac4de9f266e709642b4128dabbd4bb76a", null ],
    [ "NAME", "class_data_1_1_p_r_o_d_u_c_t.html#a91f5b8b40d2daa7c97d5ffdb37f1399f", null ],
    [ "PRICE", "class_data_1_1_p_r_o_d_u_c_t.html#a2e0827c8c644350f6c14712d97ae9870", null ],
    [ "PURCHASE", "class_data_1_1_p_r_o_d_u_c_t.html#a441455e1450031f27522263483573756", null ],
    [ "STATUS", "class_data_1_1_p_r_o_d_u_c_t.html#a74ba8ef4d42b9aaea447a2ddc532a6ea", null ],
    [ "STOCK", "class_data_1_1_p_r_o_d_u_c_t.html#a504dece1a47fe791af8a3f4d9bfdefe9", null ]
];