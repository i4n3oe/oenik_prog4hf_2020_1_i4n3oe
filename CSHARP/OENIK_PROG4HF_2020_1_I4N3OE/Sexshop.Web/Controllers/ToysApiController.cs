﻿using AutoMapper;
using Sexshop.Logic;
using Sexshop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sexshop.Web.Controllers
{
    public class ToysApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ILogic logic;
        IMapper mapper;

        public ToysApiController()
        {
            logic = new ToyLogic();
            mapper = MapperFactory.CreateMapper();
        }

        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Toy> GetAll()
        {
            var toys = logic.ReadProducts();
            return mapper.Map<IList<Data.PRODUCT>, List<Models.Toy>>(toys);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneToy(int id)
        {
            bool success = logic.DeleteProduct(id);
            return new ApiResult() { OperationResult = success };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneToy(Toy toy)
        {
            logic.CreateProduct(toy.ID, toy.Name, toy.Brand, toy.Price);
            return new ApiResult() { OperationResult = true };
        }

        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneToy(Toy toy)
        {
            bool success = logic.UpdateProduct(toy.ID, toy.Name, toy.Brand, toy.Price);
            return new ApiResult() { OperationResult = success };
        }
    }
}
