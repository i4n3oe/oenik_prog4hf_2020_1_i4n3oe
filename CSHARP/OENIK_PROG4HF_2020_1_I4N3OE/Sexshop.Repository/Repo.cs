﻿// <copyright file="Repo.cs" company="I4N3OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sexshop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Sexshop.Data;

    /// <summary>
    /// This class implements the CRUD methods.
    /// </summary>
    public class Repo : IRepository
    {
        /// <inheritdoc/>
        public virtual List<CUSTOMER> CustomerData()
        {
            return DataBase.SexshopData.CUSTOMER.ToList();
        }

        /// <inheritdoc/>
        public virtual List<PRODUCT> ProductData()
        {
            return DataBase.SexshopData.PRODUCT.ToList();
        }

        /// <inheritdoc/>
        public virtual List<REVIEW> ReviewData()
        {
            return DataBase.SexshopData.REVIEW.ToList();
        }

        /// <inheritdoc/>
        public virtual List<PURCHASE> PurchaseData()
        {
            return DataBase.SexshopData.PURCHASE.ToList();
        }

        /// <inheritdoc/>
        public virtual void Create(string table, string[] args)
        {
            switch (table)
            {
                case "CUSTOMER":
                    CUSTOMER tempCustomer = new CUSTOMER
                    {
                        CUSTOMERID = int.Parse(args[0]),
                        NAME = args[1],
                        MAIL = args[2],
                        ADDRESS = args[3],
                        PHONE = args[4],
                        BIRTHDATE = DateTime.Parse(args[5]),
                    };
                    DataBase.SexshopData.CUSTOMER.Add(tempCustomer);
                    DataBase.SexshopData.SaveChanges();
                    break;

                case "PRODUCT":
                    bool validID = false;
                    while (!validID)
                    {
                        if (DataBase.SexshopData.PRODUCT.Find(int.Parse(args[0])) != null)
                        {
                            args[0] = (int.Parse(args[0]) + 1).ToString();
                        }
                        else
                        {
                            validID = true;
                        }
                    }

                    PRODUCT tempProduct = new PRODUCT
                    {
                        BARCODE = int.Parse(args[0]),
                        NAME = args[1],
                        BRAND = args[2],
                        PRICE = int.Parse(args[3]),
                        STOCK = int.Parse(args[4]),
                        STATUS = args[5],
                    };
                    DataBase.SexshopData.PRODUCT.Add(tempProduct);
                    DataBase.SexshopData.SaveChanges();
                    break;

                case "REVIEW":
                    REVIEW tempReview = new REVIEW
                    {
                        REVIEWID = int.Parse(args[0]),
                        CUSTOMERID = int.Parse(args[1]),
                        BARCODE = int.Parse(args[2]),
                        TITLE = args[3],
                        MESSAGE = args[4],
                        CREATED = DateTime.Parse(args[5]),
                        RATING = int.Parse(args[6]),
                    };
                    DataBase.SexshopData.REVIEW.Add(tempReview);
                    DataBase.SexshopData.SaveChanges();
                    break;

                case "PURCHASE":
                    PURCHASE tempPurchase = new PURCHASE
                    {
                        PURCHASEID = int.Parse(args[0]),
                        CUSTOMERID = int.Parse(args[1]),
                        BARCODE = int.Parse(args[2]),
                        ORDERDATE = DateTime.Parse(args[3]),
                        MESSAGE = args[4],
                        PRIVACY = args[5],
                    };
                    DataBase.SexshopData.PURCHASE.Add(tempPurchase);
                    DataBase.SexshopData.SaveChanges();
                    break;

                default: throw new Exception();
            }
        }

        /// <inheritdoc/>
        public PRODUCT ReadProduct(int id)
        {
            return this.ProductData().Where(x => x.BARCODE == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public CUSTOMER ReadCustomer(int id)
        {
            return this.CustomerData().Where(x => x.CUSTOMERID == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public REVIEW ReadReview(int id)
        {
            return this.ReviewData().Where(x => x.REVIEWID == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public PURCHASE ReadPurchase(int id)
        {
            return this.PurchaseData().Where(x => x.PURCHASEID == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public virtual void Update(string table, string[] args)
        {
            switch (table)
            {
                case "CUSTOMER":
                    CUSTOMER tempCustomer = DataBase.SexshopData.CUSTOMER.Find(args[0]);
                    tempCustomer.CUSTOMERID = int.Parse(args[0]);
                    tempCustomer.NAME = args[1];
                    tempCustomer.MAIL = args[2];
                    tempCustomer.ADDRESS = args[3];
                    tempCustomer.PHONE = args[4];
                    tempCustomer.BIRTHDATE = DateTime.Parse(args[5]);
                    DataBase.SexshopData.SaveChanges();
                    break;

                case "PRODUCT":
                    PRODUCT tempProduct = DataBase.SexshopData.PRODUCT.Find(int.Parse(args[0]));
                    if (args[1] != string.Empty)
                    {
                        tempProduct.NAME = args[1];
                    }

                    if (args[2] != string.Empty)
                    {
                        tempProduct.BRAND = args[2];
                    }

                    if (int.Parse(args[3]) != 0)
                    {
                        tempProduct.PRICE = int.Parse(args[3]);
                    }

                    if (int.Parse(args[4]) != 0)
                    {
                        tempProduct.STOCK = int.Parse(args[4]);
                    }

                    if (args[5] != string.Empty)
                    {
                        tempProduct.STATUS = args[5];
                    }

                    DataBase.SexshopData.SaveChanges();
                    break;

                case "REVIEW":
                    REVIEW tempReview = DataBase.SexshopData.REVIEW.Find(args[0]);
                    tempReview.CUSTOMERID = int.Parse(args[1]);
                    tempReview.BARCODE = int.Parse(args[2]);
                    tempReview.TITLE = args[3];
                    tempReview.MESSAGE = args[4];
                    tempReview.CREATED = DateTime.Parse(args[5]);
                    tempReview.RATING = int.Parse(args[6]);
                    DataBase.SexshopData.SaveChanges();
                    break;

                case "PURCHASE":
                    PURCHASE tempPurchase = DataBase.SexshopData.PURCHASE.Find(args[0]);
                    tempPurchase.CUSTOMERID = int.Parse(args[1]);
                    tempPurchase.BARCODE = int.Parse(args[2]);
                    tempPurchase.ORDERDATE = DateTime.Parse(args[3]);
                    tempPurchase.MESSAGE = args[4];
                    tempPurchase.PRIVACY = args[5];
                    DataBase.SexshopData.SaveChanges();
                    break;

                default: throw new Exception();
            }
        }

        /// <inheritdoc/>
        public virtual void Delete(string table, int id)
        {
            switch (table)
            {
                case "CUSTOMER":
                    foreach (var item in DataBase.SexshopData.REVIEW.Where(x => x.CUSTOMERID == id).ToList())
                    {
                        DataBase.SexshopData.REVIEW.Remove(item as REVIEW);
                    }

                    foreach (var item in DataBase.SexshopData.PURCHASE.Where(x => x.CUSTOMERID == id).ToList())
                    {
                        DataBase.SexshopData.PURCHASE.Remove(item as PURCHASE);
                    }

                    DataBase.SexshopData.CUSTOMER.Remove(DataBase.SexshopData.CUSTOMER.Find(id));
                    DataBase.SexshopData.SaveChanges();
                    break;

                case "PRODUCT":
                    foreach (var item in DataBase.SexshopData.PURCHASE.Where(x => x.BARCODE == id).ToList())
                    {
                        DataBase.SexshopData.PURCHASE.Remove(item as PURCHASE);
                    }

                    DataBase.SexshopData.PRODUCT.Remove(DataBase.SexshopData.PRODUCT.Find(id));
                    DataBase.SexshopData.SaveChanges();
                    break;

                case "REVIEW":
                    DataBase.SexshopData.REVIEW.Remove(DataBase.SexshopData.REVIEW.Find(id));
                    DataBase.SexshopData.SaveChanges();
                    break;

                case "PURCHASE":
                    DataBase.SexshopData.PURCHASE.Remove(DataBase.SexshopData.PURCHASE.Find(id));
                    DataBase.SexshopData.SaveChanges();
                    break;

                default: throw new Exception();
            }
        }
    }
}
