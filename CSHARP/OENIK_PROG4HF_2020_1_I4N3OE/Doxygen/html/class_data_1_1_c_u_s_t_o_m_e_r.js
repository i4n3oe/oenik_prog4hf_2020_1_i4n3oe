var class_data_1_1_c_u_s_t_o_m_e_r =
[
    [ "CUSTOMER", "class_data_1_1_c_u_s_t_o_m_e_r.html#a5746ba23b05751b836e590d670cbe044", null ],
    [ "ADDRESS", "class_data_1_1_c_u_s_t_o_m_e_r.html#a4d6fd6a245762fed9c51045d9b316a79", null ],
    [ "BIRTHDATE", "class_data_1_1_c_u_s_t_o_m_e_r.html#a4770d57c433683f8cbc8a34870f636c8", null ],
    [ "CUSTOMERID", "class_data_1_1_c_u_s_t_o_m_e_r.html#ac9da7faa667ca04970b50e43ddb15185", null ],
    [ "MAIL", "class_data_1_1_c_u_s_t_o_m_e_r.html#a447794cc0e14a4302ffeeeb047ea8d72", null ],
    [ "NAME", "class_data_1_1_c_u_s_t_o_m_e_r.html#a0f8309ba9b5d3384ec353bc9782732ed", null ],
    [ "PHONE", "class_data_1_1_c_u_s_t_o_m_e_r.html#a7b74c190310d65acb79f9f615e00c023", null ],
    [ "PURCHASE", "class_data_1_1_c_u_s_t_o_m_e_r.html#a2016181f9ca64b2c98fa15a2cf6ea4d3", null ],
    [ "REVIEW", "class_data_1_1_c_u_s_t_o_m_e_r.html#ae9aa5a4af306d611b29e1f145140cb68", null ]
];