﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sexshop.Wpf
{
    class ToyVM : ObservableObject
    {
		private int id;
		private string brand;
		private string name;
		private int price;

		public int Price
		{
			get { return price; }
			set { price = value; }
		}


		public string Name
		{
			get { return name; }
			set { name = value; }
		}


		public string Brand
		{
			get { return brand; }
			set { brand = value; }
		}


		public int Id
		{
			get { return id; }
			set { Set(ref id, value); }
		}

		public void CopyFrom(ToyVM other)
		{
			if (other == null) return;
			this.Id = other.Id;
			this.Brand = other.Brand;
			this.Name = other.Name;
			this.Price = other.Price;
		}
	}
}
