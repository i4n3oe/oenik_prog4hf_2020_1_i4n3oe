﻿// <copyright file="ILogic.cs" company="I4N3OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sexshop.Logic
{
    using System.Collections.Generic;
    using Sexshop.Data;

    /// <summary>
    /// Interface for all methods.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// This method calls the Create method from the Repository whit the given args.
        /// </summary>
        /// <param name="table">The table where the entity takes place.</param>
        /// <param name="args">The parameters of the entity.</param>
        void Create(string table, string[] args);

        /// <summary>
        /// This method calls the ReadAllTable method from the Repository whit the given args.
        /// </summary>
        /// <returns>Returns all table in string.</returns>
        List<List<object>> ReadAllTable();

        /// <summary>
        /// Updates an entity.
        /// </summary>
        /// <param name="table">The table where the entity takes place.</param>
        /// <param name="args">The parameters of the entity.</param>
        void Update(string table, string[] args);

        /// <summary>
        /// Deletes an entity.
        /// </summary>
        /// <param name="table">The table where the entity takes place.</param>
        /// <param name="id">ID of the entity.</param>
        void Delete(string table, int id);

        /// <summary>
        /// This method gives back the most selled toy.
        /// </summary>
        /// <returns>Returns the product information and purchase count.</returns>
        string TopSellerSexToy();

        /// <summary>
        /// This method gives back the customer with the most reviews.
        /// </summary>
        /// <returns>Returns the customer information and review count.</returns>
        string MostActiveReviewer();

        /// <summary>
        /// This method gives back the number of purchases in each year.
        /// </summary>
        /// <returns>Returns the years with their purchase count.</returns>
        string SexToysSoldByYear();

        /// <summary>
        /// Gets the java endpoint.
        /// </summary>
        /// <param name="toy">the requested parameter.</param>
        /// <returns>Returns with the response.</returns>
        string GetToyColorFromJavaEndPoint(string toy);

        /// <summary>
        /// Gets a product by id.
        /// </summary>
        /// <param name="id">The product's id.</param>
        /// <returns>The produsct.</returns>
        PRODUCT ReadProduct(int id);

        /// <summary>
        /// Gets a customer by id.
        /// </summary>
        /// <param name="id">The customer's id.</param>
        /// <returns>The customer.</returns>
        CUSTOMER ReadCustomer(int id);

        /// <summary>
        /// Gets a review by id.
        /// </summary>
        /// <param name="id">The review's id.</param>
        /// <returns>The review.</returns>
        REVIEW ReadReview(int id);

        /// <summary>
        /// Gets a purchase by id.
        /// </summary>
        /// <param name="id">The purchase's id.</param>
        /// <returns>The purchase.</returns>
        PURCHASE ReadPurchase(int id);

        /// <summary>
        /// Gets Product table.
        /// </summary>
        /// <returns>Product list.</returns>
        List<PRODUCT> ReadProducts();

        /// <summary>
        /// Gets Customer table.
        /// </summary>
        /// <returns>Customer list.</returns>
        List<CUSTOMER> ReadCustomers();

        /// <summary>
        /// Gets Review table.
        /// </summary>
        /// <returns>Review list.</returns>
        List<REVIEW> ReadReviews();

        /// <summary>
        /// Gets Purchase table.
        /// </summary>
        /// <returns>Purchase list.</returns>
        List<PURCHASE> ReadPurchases();

        /// <summary>
        /// Deletes a product.
        /// </summary>
        /// <param name="id">The product's id.</param>
        /// <returns>The delete status.</returns>
        bool DeleteProduct(int id);

        /// <summary>
        /// Creates a new product.
        /// </summary>
        /// <param name="barcode">The barcode.</param>
        /// <param name="name">The name.</param>
        /// <param name="brand">The brand.</param>
        /// <param name="price">The price.</param>
        /// <param name="stock">The stock.</param>
        /// <param name="status">The status.</param>
        void CreateProduct(int barcode, string name, string brand, int price, int stock = 0, string status = "");

        /// <summary>
        /// Updates a product.
        /// </summary>
        /// <param name="barcode">The barcode.</param>
        /// <param name="name">The name.</param>
        /// <param name="brand">The brand.</param>
        /// <param name="price">The price.</param>
        /// <param name="stock">The stock.</param>
        /// <param name="status">The status.</param>
        /// <returns>The update state.</returns>
        bool UpdateProduct(int barcode, string name, string brand, int price, int stock = 0, string status = "");
    }
}
