package SexShopPck;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "SexShopServlet", urlPatterns = {"/SexShopServlet"})
public class SexShopServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            request.setCharacterEncoding("UTF8");
            response.setCharacterEncoding("UTF8");
            String toy = request.getParameter("toy");
            SexShopDataBase toys = new SexShopDataBase();
            out.println(toys.getColor(toy));
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        request.setCharacterEncoding("UTF8");
        response.setCharacterEncoding("UTF8");
        String toy = request.getParameter("toy");
        SexShopDataBase toys = new SexShopDataBase();
        PrintWriter out = response.getWriter();
        try{
        out.print(toys.getColor(toy));
        }finally{
        out.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        doGet(request,response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}