﻿// <copyright file="ToyLogic.cs" company="I4N3OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sexshop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using Sexshop.Data;
    using Sexshop.Repository;

    /// <summary>
    /// This class connects the Repository with the Main Program.
    /// </summary>
    public class ToyLogic : ILogic
    {
        private IRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToyLogic"/> class.
        /// </summary>
        public ToyLogic()
        {
            this.repository = new Repo();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToyLogic"/> class. For testing.
        /// </summary>
        /// <param name="repo">The repository to be used for testing.</param>
        public ToyLogic(IRepository repo)
        {
            this.repository = repo;
        }

        /// <inheritdoc/>
        public void Create(string table, string[] args)
        {
            this.repository.Create(table, args);
        }

        /// <inheritdoc/>
        public void CreateProduct(int barcode, string name, string brand, int price, int stock = 0, string status = "")
        {
            string[] args = { barcode.ToString(), name, brand, price.ToString(), stock.ToString(), status };
            this.Create("PRODUCT", args);
        }

        /// <inheritdoc/>
        public bool UpdateProduct(int barcode, string name, string brand, int price, int stock = 0, string status = "")
        {
            string[] args = { barcode.ToString(), name, brand, price.ToString(), stock.ToString(), status };
            this.Update("PRODUCT", args);
            if (this.ReadProduct(barcode) != null)
            {
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public PRODUCT ReadProduct(int id)
        {
            return this.repository.ReadProduct(id);
        }

        /// <inheritdoc/>
        public CUSTOMER ReadCustomer(int id)
        {
            return this.repository.ReadCustomer(id);
        }

        /// <inheritdoc/>
        public REVIEW ReadReview(int id)
        {
            return this.repository.ReadReview(id);
        }

        /// <inheritdoc/>
        public PURCHASE ReadPurchase(int id)
        {
            return this.repository.ReadPurchase(id);
        }

        /// <inheritdoc/>
        public List<PRODUCT> ReadProducts()
        {
            return this.repository.ProductData();
        }

        /// <inheritdoc/>
        public List<CUSTOMER> ReadCustomers()
        {
            return this.repository.CustomerData();
        }

        /// <inheritdoc/>
        public List<REVIEW> ReadReviews()
        {
            return this.repository.ReviewData();
        }

        /// <inheritdoc/>
        public List<PURCHASE> ReadPurchases()
        {
            return this.repository.PurchaseData();
        }

        /// <inheritdoc/>
        public List<List<object>> ReadAllTable()
        {
            List<List<object>> allTableList = null;

            return allTableList;
        }

        /// <inheritdoc/>
        public void Update(string table, string[] args)
        {
            this.repository.Update(table, args);
        }

        /// <inheritdoc/>
        public bool DeleteProduct(int id)
        {
            this.repository.Delete("PRODUCT", id);
            return true;
        }

        /// <inheritdoc/>
        public void Delete(string table, int id)
        {
            this.repository.Delete(table, id);
        }

        /// <inheritdoc/>
        public string TopSellerSexToy()
        {
            List<PURCHASE> table = new List<PURCHASE>();
            foreach (var item in this.repository.PurchaseData())
            {
                table.Add(item as PURCHASE);
            }

            var solution = table.GroupBy(x => x.BARCODE).Select(x => new
            {
                BARCODE = x.ElementAtOrDefault(0).BARCODE,
                COUNT = x.Count(),
            }).OrderByDescending(x => x.COUNT).FirstOrDefault();

            return this.ReadProduct(int.Parse(solution.BARCODE.ToString())) + " COUNT: " + solution.COUNT;
        }

        /// <inheritdoc/>
        public string MostActiveReviewer()
        {
            List<REVIEW> table = new List<REVIEW>();
            foreach (var item in this.repository.ReviewData())
            {
                table.Add(item as REVIEW);
            }

            var solution = table.GroupBy(x => x.CUSTOMERID).Select(x => new
            {
                CUSTOMERID = x.ElementAtOrDefault(0).CUSTOMERID,
                COUNT = x.Count(),
            }).OrderByDescending(x => x.COUNT).FirstOrDefault();

            return this.ReadCustomer(int.Parse(solution.CUSTOMERID.ToString())) + " COUNT: " + solution.COUNT;
        }

        /// <inheritdoc/>
        public string SexToysSoldByYear()
        {
            List<PURCHASE> table = new List<PURCHASE>();
            foreach (var item in this.repository.PurchaseData())
            {
                table.Add(item as PURCHASE);
            }

            var solution = table.GroupBy(x => x.ORDERDATE.GetValueOrDefault().Year).Select(x => new
            {
                YEAR = x.ElementAtOrDefault(0).ORDERDATE.GetValueOrDefault().Year,
                COUNT = x.Count(),
            });

            string returnString = string.Empty;
            foreach (var item in solution)
            {
                returnString += "YEAR: " + item.YEAR + " SOLD: " + item.COUNT + Environment.NewLine;
            }

            return returnString;
        }

        /// <inheritdoc/>
        public string GetToyColorFromJavaEndPoint(string toy)
        {
            string color = string.Empty;
            string url = @"http://localhost:8080/SexShop/SexShopServlet?toy=" + toy;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                {
                    color = reader.ReadToEnd();
                }
            }
            catch
            {
                color = "Nem lehet kapcsolódni a szerverhez.";
            }

            return color;
        }

        /// <summary>
        /// This method converts an object into a text.
        /// </summary>
        /// <param name="item">The object to convert.</param>
        /// <returns>Returns the given object in string.</returns>
        private string[] ReturnAnItem(object item)
        {
            string type = string.Empty;
            if (item is CUSTOMER)
            {
                type = "CUSTOMER";
            }

            if (item is PRODUCT)
            {
                type = "PRODUCT";
            }

            if (item is REVIEW)
            {
                type = "REVIEW";
            }

            if (item is PURCHASE)
            {
                type = "PURCHASE";
            }

            switch (type)
            {
                case "CUSTOMER":
                    CUSTOMER tempCustomer = item as CUSTOMER;
                    return new string[] { tempCustomer.CUSTOMERID.ToString(), tempCustomer.NAME, tempCustomer.MAIL, tempCustomer.ADDRESS, tempCustomer.PHONE, tempCustomer.BIRTHDATE.ToString() };

                case "PRODUCT":
                    PRODUCT tempProduct = item as PRODUCT;
                    return new string[] { tempProduct.BARCODE.ToString(), tempProduct.NAME, tempProduct.BRAND, tempProduct.PRICE.ToString(), tempProduct.STOCK.ToString(), tempProduct.STATUS };

                case "REVIEW":
                    REVIEW tempReview = item as REVIEW;
                    return new string[] { tempReview.REVIEWID.ToString(), tempReview.CUSTOMERID.ToString(), tempReview.BARCODE.ToString(), tempReview.TITLE, tempReview.MESSAGE, tempReview.CREATED.ToString(), tempReview.RATING.ToString() };

                case "PURCHASE":
                    PURCHASE tempPurchase = item as PURCHASE;
                    return new string[] { tempPurchase.PURCHASEID.ToString(), tempPurchase.CUSTOMERID.ToString(), tempPurchase.BARCODE.ToString(), tempPurchase.ORDERDATE.ToString(), tempPurchase.MESSAGE, tempPurchase.PRIVACY };

                default: throw new Exception();
            }
        }
    }
}
