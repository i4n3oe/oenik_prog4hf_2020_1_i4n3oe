﻿// <copyright file="DataBase.cs" company="I4N3OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sexshop.Data
{
    /// <summary>
    /// Class for accessing database.
    /// </summary>
    public static class DataBase
    {
        /// <summary>
        /// The access point of the database.
        /// </summary>
        private static SexshopDbEntities sexshopData = new SexshopDbEntities();

        /// <summary>
        /// Gets or sets the database property.
        /// </summary>
        public static SexshopDbEntities SexshopData { get => sexshopData; set => sexshopData = value; }
    }
}