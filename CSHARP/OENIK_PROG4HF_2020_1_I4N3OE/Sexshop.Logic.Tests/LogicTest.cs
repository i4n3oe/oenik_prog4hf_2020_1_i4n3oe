﻿// <copyright file="LogicTest.cs" company="I4N3OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sexshop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using Sexshop.Data;
    using Moq;
    using NUnit.Framework;
    using Sexshop.Repository;

    /// <summary>
    /// Logic testing class.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private ToyLogic testLogic;
        private Mock<Repo> mockRepository;

        private List<CUSTOMER> customerList;
        private List<PRODUCT> productList;
        private List<REVIEW> reviewList;
        private List<PURCHASE> purchaseList;

        /// <summary>
        /// Test setup.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.mockRepository = new Mock<Repo>();

            this.customerList = new List<CUSTOMER>() { new CUSTOMER() { CUSTOMERID = 10, NAME = "name1", MAIL = "mail@mail.com", ADDRESS = "address1", PHONE = "0650-123-4567", BIRTHDATE = DateTime.Parse("27-MAR-1997") } };
            this.productList = new List<PRODUCT>() { new PRODUCT() { BARCODE = 20, NAME = "toy1", BRAND = "brand1", PRICE = 100, STOCK = 1, STATUS = "used" } };
            this.reviewList = new List<REVIEW>() { new REVIEW() { REVIEWID = 30, CUSTOMERID = 10, BARCODE = 20, TITLE = "title1", MESSAGE = "message1", CREATED = DateTime.Parse("30-MAR-2018"), RATING = 5 } };
            this.purchaseList = new List<PURCHASE>() { new PURCHASE() { PURCHASEID = 40, CUSTOMERID = 10, BARCODE = 20, ORDERDATE = DateTime.Parse("27-MAR-2018"), PRIVACY = "private" } };

            this.mockRepository.Setup(x => x.CustomerData()).Returns(this.customerList);
            this.mockRepository.Setup(x => x.ProductData()).Returns(this.productList);
            this.mockRepository.Setup(x => x.ReviewData()).Returns(this.reviewList);
            this.mockRepository.Setup(x => x.PurchaseData()).Returns(this.purchaseList);

            this.testLogic = new ToyLogic(this.mockRepository.Object);
        }

        /// <summary>
        /// create an entity test.
        /// </summary>
        [Test]
        public void Test_Create()
        {
            string[] args = new string[6] { "11", "name2", "mail2@mail.com", "address2", "0650-123-4568", "28-MAR-1997" };

            this.mockRepository.Setup(x => x.Create("CUSTOMER", args)).Verifiable();

            this.testLogic.Create("CUSTOMER", args);

            this.mockRepository.Verify(x => x.Create("CUSTOMER", args));
        }

        /// <summary>
        /// Read an entity test.
        /// </summary>
        [Test]
        public void Test_Read()
        {
            //Assert.That(this.testLogic.ReadItem("CUSTOMER", 10).Split(',')[0] == "CUSTOMERID: 10");
        }

        /// <summary>
        /// Update an entity test.
        /// </summary>
        [Test]
        public void Test_Update()
        {
            string[] args = new string[6] { "10", "name2", "mail2@mail.com", "address2", "0650-123-4568", "28-MAR-1997" };

            this.mockRepository.Setup(x => x.Update("CUSTOMER", args)).Verifiable();

            this.testLogic.Update("CUSTOMER", args);

            this.mockRepository.Verify(x => x.Update("CUSTOMER", args));
        }

        /// <summary>
        /// Delete an entity test.
        /// </summary>
        [Test]
        public void Test_Delete()
        {
            string[] args = new string[6] { "11", "name2", "mail2@mail.com", "address2", "0650-123-4568", "28-MAR-1997" };
            this.mockRepository.Setup(x => x.Create("CUSTOMER", args));

            this.testLogic.Delete("CUSTOMER", 11);

            //Assert.That(
            //    () => this.testLogic.ReadItem("CUSTOMER", 11),
            //    Throws.Exception
            //  .TypeOf<Exception>());
        }

        /// <summary>
        /// Read a table test.
        /// </summary>
        [Test]
        public void Test_ReadTable()
        {
            //Assert.That(this.testLogic.ReadTable("PRODUCT").Split(',')[0] == "BARCODE: 20");
        }

        /// <summary>
        /// Read all table test.
        /// </summary>
        [Test]
        public void Test_ReadAllTable()
        {
            //Assert.That(this.testLogic.ReadAllTable().Split(',')[0] == "CUSTOMERID: 10");
        }

        /// <summary>
        /// Top selled toy test.
        /// </summary>
        [Test]
        public void Test_TopSellerToy()
        {
            Assert.That(this.testLogic.TopSellerSexToy().Contains("toy1"));
        }

        /// <summary>
        /// Most active reviewer test.
        /// </summary>
        [Test]
        public void Test_MostActiveReviewer()
        {
            Assert.That(this.testLogic.MostActiveReviewer().Contains("CUSTOMERID: 10"));
        }
    }
}
