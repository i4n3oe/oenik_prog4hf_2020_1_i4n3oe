﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Sexshop.ConsoleClient
{
    public class Toy
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public int Price { get; set; }
        public override string ToString()
        {
            return $"ID={ID}\tBrand={Brand}\tPrice={Price}\tName={Name}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            string url = "http://localhost:58142/api/ToysApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Toy>>(json);
                foreach (var item in list) Console.WriteLine(item.ToString());
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Toy.Brand), "BRAND1");
                postData.Add(nameof(Toy.Name), "Holy Rod");
                postData.Add(nameof(Toy.Price), "120");
                postData.Add(nameof(Toy.ID), "19");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int toyID = JsonConvert.DeserializeObject<List<Toy>>(json).Single(x => x.Name == "Holy Rod").ID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Toy.ID), toyID.ToString());
                postData.Add(nameof(Toy.Brand), "BRAND1");
                postData.Add(nameof(Toy.Name), "Holy Rod");
                postData.Add(nameof(Toy.Price), "120");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + toyID).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }

        }
    }
}
