﻿// <copyright file="IRepository.cs" company="I4N3OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sexshop.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using Sexshop.Data;

    /// <summary>
    /// Interface for CRUD methods.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Gets Customer data.
        /// </summary>
        /// <returns>Customer list.</returns>
        List<CUSTOMER> CustomerData();

        /// <summary>
        /// Gets Product data.
        /// </summary>
        /// <returns>Product list.</returns>
        List<PRODUCT> ProductData();

        /// <summary>
        /// Gets Review data.
        /// </summary>
        /// <returns>Review list.</returns>
        List<REVIEW> ReviewData();

        /// <summary>
        /// Gets Purchase data.
        /// </summary>
        /// <returns>Purchase list.</returns>
        List<PURCHASE> PurchaseData();

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="table">The table where the instance will take place.</param>
        /// <param name="args">The parameters of the instance.</param>
        void Create(string table, string[] args);

        /// <summary>
        /// This method updates an entity with the new parameters.
        /// </summary>
        /// <param name="table">The table where the entity takes place.</param>
        /// <param name="args">The parameters of the entity.</param>
        void Update(string table, string[] args);

        /// <summary>
        /// This method delete the instance with the given ID.
        /// </summary>
        /// <param name="table">The table where the instance will take place.</param>
        /// <param name="id">ID of the instance.</param>
        void Delete(string table, int id);

        /// <summary>
        /// Gets a product by id.
        /// </summary>
        /// <param name="id">The product's id.</param>
        /// <returns>The product.</returns>
        PRODUCT ReadProduct(int id);

        /// <summary>
        /// Gets a customer by id.
        /// </summary>
        /// <param name="id">The customer's id.</param>
        /// <returns>The customer.</returns>
        CUSTOMER ReadCustomer(int id);

        /// <summary>
        /// Gets a review by id.
        /// </summary>
        /// <param name="id">The review's id.</param>
        /// <returns>The review.</returns>
        REVIEW ReadReview(int id);

        /// <summary>
        /// Gets a purchase by id.
        /// </summary>
        /// <param name="id">The purchase's id.</param>
        /// <returns>The purchase.</returns>
        PURCHASE ReadPurchase(int id);
    }
}
